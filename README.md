# Intern test Bosch

# Đề thi
> Ngôn ngữ: Python <br/>
> Mục đích: kiểm tra idiomatic Python <br/>
> Cho một chuỗi s chứa họ & tên, tách chuỗi thành các từ (mảng 1 chiều), và tách từ thành từng kí tự (mảng 2 chiều) <br/>
> Input: string s = "Nguyen Duy Quang" <br/>
> Output: [][]string res = [["N,"g","u"]] .....

## Solution

- Ở đây là một bài tập thuần tuý muốn kiểm tra khả năng vận dụng ngôn ngữ Python chứ ko phải kiểm tra tư duy logic CTDL thuật toán
- Đánh giá: Must be a troll, there's nothing to judge here

## Contribute

- Đề test này không phải của mình, do bạn mình làm bên mảng data science cung cấp
- Credit by Vu Le